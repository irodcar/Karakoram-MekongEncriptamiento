/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encriptaciontrabajo;

/**
 *
 * @author Rod
 */
public class Encriptador {
    int p;
    public Encriptador(){
    }
    
    void setP(int p){
        this.p = p;
    }
    
    public  String Encriptar(String ascii_input) {
        String new_code= "";
        for(int i=0;i<ascii_input.length();i++){
           int ascii_code = (int)ascii_input.substring(i, i+1).charAt(0);
           //System.out.println("Encriptando: "+ascii_input.substring(i, i+1) );
           //System.out.println("code : "+ascii_code);
           int cociente = ascii_code/16;
           int residuo = ascii_code%(16);
           //System.out.println("cociente: " +cociente);
           //System.out.println("residuo: " +residuo);
           int inverso_q = Inverso(cociente);
           int inverso_r = Inverso(residuo);
           
           //System.out.println("inverso q : "+inverso_q + " base2: " + toBinary(inverso_q));
           //System.out.println("inverso r : "+inverso_r+ " base2: " + toBinary(inverso_r));
           
           String hex_n = toBinary(inverso_q)  + toBinary(inverso_r);
           
           
           int ascii_output = Integer.valueOf(OrganizarFormato(hex_n), 2);
           //System.out.println("hex: " + hex_n + " "+ OrganizarFormato(hex_n) +  " dec: " +ascii_output);
           //System.out.println("output:" + (char)ascii_output);
           char c = (char)ascii_output;
           new_code+=String.valueOf(c);
        }
        
        return new_code;
    }    

    public String Desencriptar(String ascii_input) {
        String source = "";
            for(int i=0;i<ascii_input.length();i++){
                int ascii_code = (int)ascii_input.substring(i, i+1).charAt(0);
                //System.out.println("Desencriptando: "+ ascii_input.substring(i, i+1));
                String base16 = Integer.toHexString(ascii_code);
                int base10 = Integer.valueOf(base16, 16);
                String base2 = Integer.toBinaryString(base10);
                
                if(base2.length()!=8){
                    do{
                        base2="0"+base2;
                    }while(base2.length()<8);
                } 
                //System.out.println(base2);
                //falta revisar el metodo de desecnriptar!
                String inverso_a_base2 = base2.substring(0,4);
                String inverso_b_base2 = base2.substring(4,8);
                //System.out.println(inverso_a_base2 + " " +inverso_b_base2);
                int inverso_a = Integer.valueOf(OrganizarFormato(inverso_a_base2), 2);
                int inverso_b = Integer.valueOf(OrganizarFormato(inverso_b_base2), 2);
                //System.out.println("iq: "+inverso_a+ " ir: " + inverso_b);
                int cociente = Inverso(inverso_a);
                int residuo = Inverso(inverso_b);
                //System.out.println("q: "+cociente+ " r: " + residuo);
                
                
                int ascii_code_des = 16*cociente + residuo;
                char c = (char)ascii_code_des;
                String s_source = String.valueOf(c);
                //System.out.println("des: "+ s_source);
                source+=s_source;
            }
        return source;
    }
    
    private int Inverso(int cociente) {
      int inverso=-1;
      
      for(int i=0;i<400;i++){
          if((cociente*i)%(p)==1){
              return i;
          }
        
      }
      
      if(cociente==0){
          return 0;
      }
      
      return inverso;
    }
    
        private  String toBinary(int n)
    {
        String b = ""; // binary representation as a string
        while (n != 0) {
            int r = (int)(n % 2); // remainder
            b = r + b; // concatenate remainder
            n /= 2; // reduce n
        }
        if(b.length()!=4){
        do{
            b="0"+b;
        }while(b.length()<4);
        }
        return b;
    }

    private  String OrganizarFormato(String hex_n) {
        String formato=hex_n;
        
        while(formato.startsWith("0")){
            formato=formato.substring(1,formato.length());
        }
        
        if(formato.equals("")){
            return "0";
        }
        return formato;
    }
}
